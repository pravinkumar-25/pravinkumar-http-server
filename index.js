const http = require("http");
const fs = require("fs");
const { v4: uuidv4 } = require("uuid");

const server = http.createServer((request, response) => {
	let requestUrl = request.url;
	if (requestUrl === "/") {
		response.writeHead(404, { "Content-Type": "text/html" });
		response.write(
			"Empty request: " +
			"Valide requests are /html, /json, /status/{STATUS_CODES} and /delay/{delayTime}"
		);
		response.end();
	}

	else if (requestUrl === "/html") {
		fs.readFile("./index.html", "utf-8", (err, data) => {
			if (err) {
				response.writeHead(404, { "Content-Type": "text/html" });
				response.write("An error occurred");
				response.end();
			} else {
				response.writeHead(200, { "Content-Type": "text/html" });
				response.write(data);
				response.end();
			}
		});
	}

	else if (requestUrl === "/json") {
		fs.readFile("./data.json", "utf-8", (err, data) => {
			if (err) {
				response.writeHead(404, { "Content-Type": "application/json" });
				response.write("An error occurred");
				response.end();
			} else {
				response.writeHead(200, { "Content-Type": "application/json" });
				response.write(data);
				response.end();
			}
		});
	}

	else if (requestUrl === "/uuid") {
		const uuid = uuidv4();
		const uuidObject = {
			uuid: uuid,
		};
		response.writeHead(200, { "Content-Type": "application/json" });
		response.write(JSON.stringify(uuidObject));
		response.end();
	}

	else if (
		requestUrl.split("/").length === 3 &&
		requestUrl.split("/")[1] === "status"
	) {
		if (requestUrl.split("/")[2] in http.STATUS_CODES) {
			const status = requestUrl.split("/")[2];
			response.writeHead(status, { "Content-Type": "text/plain" });
			response.write(http.STATUS_CODES[status]);
		} else {
			response.writeHead(400, { "Content-Type": "text/plain" });
			response.write("Wrong status code provided");
		}
		response.end();
	}

	else if (
		requestUrl.split("/").length === 3 &&
		requestUrl.split("/")[1] === "delay"
	) {
		const delay = parseInt(requestUrl.split("/")[2]);
		if (!isNaN(delay)) {
			setTimeout(() => {
				response.writeHead(200, { "Content-Type": "text/plain" });
				response.write("Delayed for " + delay + " seconds");
				response.end();
			}, delay * 1000);
		} else {
			response.writeHead(400, { "Content-Type": "text/plain" });
			response.write("Delay is not a number");
			response.end();
		}
	} else {
		response.writeHead(404, { "Content-Type": "text/plain" });
		response.write("The server has found no data on this url");
		response.end();
	}
});

server.listen(8080, (err) => {
	if(err){
		console.error(err);
	} else {
		console.log("Listening to the port 8080...");
	}
});

